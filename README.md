
<h1 align="center">Joansiitoh (VainillaTech)</h1>

Hello! I'm Joansiitoh, I've been programming for about 5 years on Minecraft and other projects for Linux!

## Languages

I am comfortable with the following langauges:

- Java and Spigot (CraftBukkit and NMS) since 4 years
- MariaDB, MySQL, MongoDB, Redis
- Php with Html.
- Python


## Check out our team website, [vainillatech.club](https://vainillatech.club "vainillatech.club")!

<div align="center">

<a href="https://github.com/joansitoh?tab=repositories" title="Repositories">
    <img height="150px" width="auto" alt="Repositories" src="https://github-readme-stats.vercel.app/api/top-langs/?username=joansitoh&exclude_repo=git-commit-spam-ex,js-utils&hide=GLSL&layout=compact&theme=radical">
</a>
<a href="https://github.com/joansitoh?tab=repositories" title="Repositories">
    <img height="150px" width="auto" alt="Repositories" src="https://github-readme-stats.vercel.app/api?username=joansitoh&show_icons=true&theme=radical">
</a>

</div>


---
### Contact me with these links:
- 🐦 Twitter: https://twitter.com/joansiitohtv
- 📧 Discord: joansiitoh#9303
---

---
### 📚 History worked projects:
- ⚔ SnowCrash Network - A Minecraft PvP Network based on Annihilation (snowcrash.us - Head Developer)
- ⚔ Annihilate - A Minecraft PvP Network based on Annihilation (annihilate.io - Head Developer)
- ⚔ ColdNetwork - A Minecraft PvP Network based on Practice (coldnetwork.us - Developer)
- ⚔ SkillWars Network - A Minecraft PvP Network based on Annihilation (skillwars.us - Head Developer)
- ⚔ Kirei - A Minecraft PvP network based on MegaWalls (kirei.gg - Head Developer)
- ⚔ Ascend Network - A Minecraft PvP and Survival Network (ascendmc.us - Head Developer)

---
